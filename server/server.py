import socket, ssl, sqlite3, logging, sys, sqlite3, types

HOST, PORT, CERT = '', 9999, 'server_finished.pem'
DB_NAME = 'AntiVirusDataBase.db'
MAX_CLIENTS = 10
ERROR_MESSAGE = 'error'


#conn		socket (connection with client)
#db_conn	connection with database

def setup_custom_logger(name):
    formatter = logging.Formatter(fmt = '%(asctime)s %(levelname)-8s %(message)s',
                                  datefmt = '%Y-%m-%d %H:%M:%S')
    handler = logging.FileHandler(name + '.log', mode = 'a+')
    handler.setFormatter(formatter)
    screen_handler = logging.StreamHandler(stream = sys.stdout)
    screen_handler.setFormatter(formatter)
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)
    logger.addHandler(handler)
    logger.addHandler(screen_handler)
    return logger
#check message

def get_sql_response(client_msg):
	db_conn = sqlite3.connect(DB_NAME)
	if(len(client_msg) < 2):
		logger.error("DATA TOO SHORT: [" + client_msg + "]")
		return ERROR_MESSAGE
	if(client_msg[0] == '1'):
		msg = "0"
		notVallid = False
		stringData = client_msg[2:]
		if len(stringData) == 0:
			logger.error("DATA Is NULL")
			return ERROR_MESSAGE
		try:
			data = eval(stringData)
		except Exception as e:
			logger.error(e)
			return ERROR_MESSAGE
		if(client_msg[1] == '0'):
			if not type(data) is list:
				logger.error("DATA TYPE NOT MATCHING")
				return ERROR_MESSAGE
			msg = msg + "2["
			for sign in data:
				cursor = db_conn.execute("SELECT COUNT(*) from appsSingatures WHERE sign == '" +  sign + "'" )
				result=cursor.fetchone()

				if(result[0] > 0 and notVallid is False):#exists
					notVallid = True
					msg = msg + "'" + sign + "'"
					#in the db
					
				elif(result[0] > 0 and notVallid is True):#exists
					msg = msg + ",'" + sign + "'"
					#in the db
			
			if(notVallid is True):
				msg = msg + "]"
				
			elif(notVallid is False):
				msg = "00"
			
			return msg
			
		elif(client_msg[1] == '1'):
			if not type(data) is dict:
				logger.error("DATA TYPE NOT MATCHING")
				return ERROR_MESSAGE
			allNotVallid = False
			msg = msg + "1{"
			for app in data:	
				notVallid = False
				for lib in data[app]:
					cursor = db_conn.execute("SELECT COUNT(*) from libs WHERE lib == '" +  lib + "'" )
					result=cursor.fetchone()

					if(result[0] > 0 and notVallid is False and allNotVallid is False):#exists
						allNotVallid = True
						notVallid = True
						msg = msg + "'" + app + "':['" + lib + "'"
						#in the db
						
					elif(result[0] > 0 and notVallid is False and allNotVallid is True):#exists
						notVallid = True
						msg = msg + ",'" + app + "':['" + lib + "'"
						#in the db	
						
					elif(result[0] > 0 and notVallid is True):#exists
						msg = msg + ",'" + lib + "'"
						#in the db
						
				if(notVallid is True):
					msg = msg + "]"

			if(allNotVallid is True):
				msg = msg + "}"
				
			elif(allNotVallid is False):
				msg = "00"		
					
			return msg

		elif(client_msg[1] == '2'):
			if not type(data) is list:
				logger.error("DATA TYPE NOT MATCHING")
				return ERROR_MESSAGE
			msg = msg + "3["
			for domain in data:
				cursor = db_conn.execute("SELECT COUNT(*) from domains WHERE domain == '" +  domain + "'" )
				result=cursor.fetchone()

				if(result[0] > 0 and notVallid is False):#exists
					notVallid = True
					msg = msg + "'" + domain + "'"
					#in the db
					
				elif(result[0] > 0 and notVallid is True):#exists
					msg = msg + ",'" + domain + "'"
					#in the db
			
			if(notVallid is True):
				msg = msg + "]"
				
			elif(notVallid is False):
				msg = "00"
			
			return msg
			
		elif(client_msg[1] == '3'):
			if not type(data) is tuple:	
				logger.error("DATA TYPE NOT MATCHING")
				return ERROR_MESSAGE
			msg = msg + "1{"
			for lib in data[1]:
				cursor = db_conn.execute("SELECT COUNT(*) from libs WHERE lib == '" +  lib + "'" )
				result=cursor.fetchone()

				if(result[0] > 0 and notVallid is False):#exists
					notVallid = True
					msg = msg + "'" + data[0] + "':['" + lib + "'"
					#in the db
					
				elif(result[0] > 0 and notVallid is True):#exists
					msg = msg + ",'" + lib + "'"
					#in the db
					
			if(notVallid is True):
				msg = msg + "]}"
				return msg			
			else:
				msg = "02["
				cursor = db_conn.execute("SELECT COUNT(*) from appsSingatures WHERE sign == '" +  data[0] + "'" )
				result=cursor.fetchone()

				if(result[0] > 0):#exists
					msg = msg + "'" + data[0] + "']"
					#in the db_conn
					
				else:
					msg = "00"
			
			return msg
				
		else:
			logger.error('INVALLID MESSAGE')
	else:
		logger.error('INVALLID MESSAGE')
	return ERROR_MESSAGE

def socket_handle(conn):
	user_addr, user_port = conn.getpeername()[0], conn.getpeername()[1]
	client_msg = conn.recv(8192*2).decode('utf-8')[:-1]
	logger.info('RECV from [%s::%d]: %s' % (user_addr, user_port, client_msg))
	msg_to_client = get_sql_response(client_msg)
	logger.info('SEND to [%s::%d]: %s' % (user_addr, user_port, msg_to_client))
	conn.write(b'%s' % msg_to_client.encode())
	logger.info('CLOSE socket with [%s::%d]' % (user_addr, user_port))
	conn.close()

def main():
	logger.debug('SERVER START RUNNING')
	db_conn = sqlite3.connect(DB_NAME)
	sock = socket.socket()
	sock.bind((HOST, PORT))
	sock.listen(MAX_CLIENTS)
	ssl_context = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
	ssl_context.load_cert_chain(certfile=CERT)
	while True:
		conn = None
		ssock, addr = sock.accept()
		try:
			conn = ssl_context.wrap_socket(ssock, server_side=True)
			socket_handle(conn)
		except ssl.SSLError as e:
			logger.error(e)
		finally:
			if conn:
				conn.close()

if __name__ == '__main__':
	logger = setup_custom_logger('server_log')
	main()