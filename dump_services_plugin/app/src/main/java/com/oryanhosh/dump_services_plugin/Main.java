package com.oryanhosh.dump_services_plugin;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Main {

    public Main() {
        //System.out.println("call to dumpCamera()");
    }

    public void dumpCamera()
    {
        Process p = null;
        try {
            p = new ProcessBuilder().command("/data/local/tmp/antimalwarefiles/dump_camera.sh").start();
            p.waitFor();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        if(p != null)
        {
            String output = convertStreamToString(p.getInputStream());
            System.out.print(output.replace('\n', ' '));
        }
        else
        {
            //System.out.println("ERROR: process is null");
        }
    }

    private String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
}
