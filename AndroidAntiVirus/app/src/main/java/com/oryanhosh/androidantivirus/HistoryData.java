package com.oryanhosh.androidantivirus;

import java.util.ArrayList;

class HistoryData {
    private ArrayList<History> historyArrayList;

    public HistoryData() {
        historyArrayList = new ArrayList<>();
    }


    public ArrayList<History> getHistoryArrayList() {
        return historyArrayList;
    }
}
