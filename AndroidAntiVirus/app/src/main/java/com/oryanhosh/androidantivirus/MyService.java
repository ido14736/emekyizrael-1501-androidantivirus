package com.oryanhosh.androidantivirus;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;


public class MyService extends Service {
    private ConnectionManagerTask connectionManager = null;

    @Override
    public void onCreate() {
        Log.i(Constants.DEBUG_TAG,"MyService onCreate running");

        createNotificationIntent();

        runGetTopActivity(); //print top activity to log

        ExecProcess process1 = new ExecProcess(this);
        process1.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,"plugin3,sniffDNS");

        checkDomainsAndDumps();

        registerPackageChangeReceiver();

    }

    /**
     * Function register Package Change Receiver
     * input/output: noun
     * */
    private void registerPackageChangeReceiver()
    {
        IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_PACKAGE_ADDED);
        filter.addAction(Intent.ACTION_PACKAGE_REMOVED);
        filter.addDataScheme("package");
        BroadcastReceiverPackageChange mReceiver = new BroadcastReceiverPackageChange();
        registerReceiver(mReceiver, filter);
    }

    /**
     * Function check top activity every 20 sec
     * input/output: noun
     * */
    private void runGetTopActivity() {
        Log.i(Constants.DEBUG_TAG, "start runGetTopActivity()");
               new Thread(new Runnable() {
            @Override
            public void run() {
                while (MainActivity.isMyServiceRunning(getBaseContext())){
                    getTopActivity(System.currentTimeMillis());
                    try {
                        Thread.sleep(2000); //wait 2 seconds
                    } catch (InterruptedException e) {
                        Log.e(Constants.DEBUG_TAG, "Error: ", e);
                    }
                }
            }
        }).start();
    }

    /***
     * Function read domains from file and check dumps
     * */
    private void checkDomainsAndDumps() {
        Log.i(Constants.DEBUG_TAG, "start checkDomainsAndDumps()");
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (MainActivity.isMyServiceRunning(getBaseContext())){
                    Log.i(Constants.DEBUG_TAG, "read domains from file");
                    //start readDomains process
                    ExecProcess process1 = new ExecProcess(MyService.this);
                    process1.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,"plugin3,readDomains");
                    //start dumpCamera process
                            ExecProcess process2 = new ExecProcess(MyService.this);
                    process2.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,"plugin4,dumpCamera");
                    try {
                        Thread.sleep(20000); //wait 20 seconds
                    } catch (InterruptedException e) {
                        Log.e(Constants.DEBUG_TAG, "Error: ", e);
                    }
                }
            }
        }).start();
    }

    /**
     * Function create new ConnectionManagerTask
     * input/output: noun
     * */
    private void createConnectionWithServer() {
        if(connectionManager  != null){ //close connection if exist
            connectionManager = null;
        }
        connectionManager = new ConnectionManagerTask(getApplicationContext());
        connectionManager.execute();
        SystemClock.sleep(3000);
    }

    /**
     * Function create app notification
     * input: status string
     * output: noun
     * */
    private void createNotificationIntent() {
        //create intent for execute notification
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,notificationIntent,0);

        //create the notification
        Notification notification = new NotificationCompat.Builder(this, NotificationBar.CHANNEL_ID)
                .setContentTitle("Android AntiVirus")
                .setContentText("Real-Time Protection mode is on")
                .setSmallIcon(R.drawable.ic_antivirus_icon)
                .setContentIntent(pendingIntent)
                .build();

        startForeground(1,notification); //start the ForegroundService (notification)
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(Constants.DEBUG_TAG,"onStartCommand running");
        String extraData = intent.getStringExtra("inputExtra");

        //check intent extra data
        handleNewMessageFromIntent(extraData.substring(0,extraData.indexOf(':')+1),
                extraData.substring(extraData.indexOf(':')+1));
        Log.i(Constants.DEBUG_TAG, "onStartCommand data: " + extraData);

        return START_STICKY;
    }

    /**
     * Function check current top activity
     * input/output: noun
     * */
    private void getTopActivity(long time) {
        String topPackageName;
        UsageStatsManager mUsageStatsManager = (UsageStatsManager) getSystemService(Context.USAGE_STATS_SERVICE);
        // We get usage stats for the last 10 seconds
        List< UsageStats > stats = mUsageStatsManager.queryUsageStats(UsageStatsManager.INTERVAL_DAILY, time - 1000 * 10, time);
        // Sort the stats by the last time used
        if (stats != null) {
            SortedMap< Long, UsageStats > mySortedMap = new TreeMap<>();
            for (UsageStats usageStats: stats) {
                mySortedMap.put(usageStats.getLastTimeUsed(), usageStats);
            }
            if (!mySortedMap.isEmpty()) { //if package found
                topPackageName = mySortedMap.get(mySortedMap.lastKey()).getPackageName();
                Constants.topApps.add(topPackageName);
                Log.i(Constants.DEBUG_TAG, "top package: " + topPackageName);
            }
        }
    }

    /**
     * Function check message type from intent
     * input: message type and data
     * output: noun
     * */
    private void handleNewMessageFromIntent(String type, String data){
        switch (type) {
            case Constants.TYPE_SEND_MESSAGE_TO_SERVER:
                sendMessageToServer(data);
                break;
            case Constants.TYPE_EXEC_APPPROCESS:
                ExecProcess process = new ExecProcess(this);
                process.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, data);
                break;
            case Constants.TYPE_CHECK_PACKAGE:
                checkNewPackage(data);
                break;
            case Constants.TYPE_INIT_MESSAGE:
                Log.i(Constants.DEBUG_TAG, "MyService is running");
                break;
            default:
                Log.e(Constants.DEBUG_TAG, "unknown message type: " + type);
                break;
        }
    }


    /**
     * Function send message to server
     * input: string message
     * output: noun
     * */
    private void sendMessageToServer(String data){
        createConnectionWithServer();
        if (connectionManager.mTcpClient == null)
        {
            Log.e(Constants.DEBUG_TAG,"connectionManager.mTcpClient is null");
        }
        try{
            //if connection made
            if (connectionManager.mTcpClient != null && connectionManager.mTcpClient.socket !=null)
            {
                if(connectionManager.mTcpClient.socket.isConnected())
                {
                    Log.i(Constants.DEBUG_TAG,"sending message to server");
                    connectionManager.mTcpClient.sendMessage(data); //send message
                }
                else{
                    Log.e(Constants.DEBUG_TAG,"TCP client socket is not connected");
                }
            }
            else{
                //check error type
                if(connectionManager.mTcpClient == null)
                {
                    connectionManager.cancel(true);
                    Log.e(Constants.DEBUG_TAG,"TCP client is null");
                }
                else if(connectionManager.mTcpClient.socket == null)
                {
                    connectionManager.cancel(true);
                    connectionManager = null;
                    Log.e(Constants.DEBUG_TAG,"TCP socket is null");
                }
            }
        }
        catch (Exception e) {
            Log.e(Constants.DEBUG_TAG, "Connection Error: ", e);
        }
    }

    /**
     * Function check new package with the server
     * input: package name
     * output: noun
     * */
    private void checkNewPackage(String pkgName)
    {
        Log.i(Constants.DEBUG_TAG, "checking package: "+ pkgName.substring(8));
        sendMessageToServer("13" + "('" + pkgName.substring(8) + "',[])");
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}
