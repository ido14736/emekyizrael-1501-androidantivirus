package com.oryanhosh.androidantivirus;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

public class MenuScanApps extends Fragment implements View.OnClickListener {
    public static ProgressDialog dialog;
    private ImageView imageViewScan;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle("SCAN APPS");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.menu_scan_apps, container, false);
        imageViewScan = view.findViewById(R.id.imageViewScanNow);
        imageViewScan.setOnClickListener(this);
        return view;
    }


    @Override
    public void onClick(View v) {
        if(v == imageViewScan)
        {
            dialog = ProgressDialog.show(getContext(), "",
                    "Scanning is in progress...\nplease wait", true);
            MainActivity.startService(Constants.TYPE_EXEC_APPPROCESS + "plugin1" + "," + "getAllApps", getContext());
        }
    }
}
