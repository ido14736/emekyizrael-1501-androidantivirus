package com.oryanhosh.androidantivirus;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.regex.Pattern;

class ExecProcess extends AsyncTask<String,String,Void> {
    private Context mContext;

    /**
     * C'tor save Context
     * */
    public ExecProcess(Context context)
    {
        mContext = context;
    }

    @Override
    protected Void doInBackground(String... strings) {
        Log.i(Constants.DEBUG_TAG, "in ExecProcess");
        Process p = null;
        try {
            //try to run the command
            p = new ProcessBuilder()
                    .command("/data/local/tmp/antimalwarefiles/run_app_process.sh", strings[0].split(",")[0],strings[0].split(",")[1])
                    .start();
            p.waitFor();
        } catch (IOException |InterruptedException e) {
            Log.e(Constants.DEBUG_TAG, "Error: ", e);
        }
        if (p != null) {
            //get command output
            String output = convertStreamToString(p.getInputStream());

            if(!output.isEmpty())
            {
                Log.i(Constants.DEBUG_TAG, "output of " + strings[0] + ": " + output);

                //check command type
                if(strings[0].split(",")[0].equals("plugin1") || strings[0].equals("plugin3,readDomains"))
                {
                    String[] out = output.split("\n"); //get last line of command output
                    String resOutput = out[out.length-1];

                    if(strings[0].equals("plugin3,readDomains") && resOutput.length() <= 7)
                    {
                        return null;
                    }

                    Intent serviceIntent = new Intent(mContext, MyService.class);
                    serviceIntent.putExtra("inputExtra",
                            Constants.TYPE_SEND_MESSAGE_TO_SERVER + resOutput);
                    ContextCompat.startForegroundService(mContext, serviceIntent);
                }
                else if(strings[0].startsWith("plugin2,"))
                {
                    Intent intent = new Intent("ServiceAndMainActivityUpdates");
                    String[] out = output.split("\n");
                    Log.i(Constants.DEBUG_TAG, "output from " + strings[0] + ": " + out[out.length-1]);
                    intent.putExtra("inputExtra", Constants.TOBROADCAST_FROM_CACHE + out[out.length-1]);
                    intent.putExtra("inputExtra", Constants.TOBROADCAST_FROM_CACHE + out[out.length-1]);
                    LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
                }
                else if(strings[0].startsWith("plugin4,"))
                {
                    Intent intent = new Intent("ServiceAndMainActivityUpdates");
                    String[] out = output.split("\n");
                    String[] connections = out[out.length-1].split(Pattern.quote("|"));
                    //split connections to string array
                    for (String conn : connections)
                    {
                        if (conn.length() < 10){
                            continue;
                        }
                        //extract data from connection
                        String time = conn.split(" ")[1].trim();
                        String date = conn.split(" ")[0].trim();
                        boolean newConn = conn.charAt(conn.indexOf("CONNECT")-1) == ' ';
                        String packageName = conn.substring(conn.indexOf("package") + 8).split(" ")[0].trim();

                        Log.i(Constants.DEBUG_TAG, "time: " + time + "\ndate: " + date + "\nnewConn:" + newConn + "\npackage: " + packageName);
                    }
                    Log.i(Constants.DEBUG_TAG, "output from " + strings[0] + ": " + out[out.length-1]);
                    intent.putExtra("inputExtra", Constants.TOBROADCAST_FROM_DUMP + out[out.length-1]);
                    LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent); //send output by Broadcast
                }
            }
        } else {
            Log.e(Constants.DEBUG_TAG, "process for " + strings[0] + " is null");
        }
        return null;
    }

    /**
     * Function convert stream output to string
     * input: InputStream
     * output: string
     * */
    private String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append('\n');
            }
        } catch (IOException e) {
            Log.e(Constants.DEBUG_TAG, "Error: ", e);
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                Log.e(Constants.DEBUG_TAG, "Error: ", e);
            }
        }
        return sb.toString();
    }
}
