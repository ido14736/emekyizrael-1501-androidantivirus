package com.oryanhosh.androidantivirus;

import android.os.AsyncTask;
import android.util.Log;

import java.io.PrintWriter;

/**
 * A simple task for sending messages across the network.
 */
class TCPMessageSendTask extends AsyncTask<Void, Void, Void> {

    private PrintWriter out;
    private String message;

    public TCPMessageSendTask(PrintWriter out, String message){
        this.out = out;
        this.message = message;
    }

    @Override
    protected Void doInBackground(Void... arg0){
        if (out != null && !out.checkError()) {
            try{
                out.println(message);
                out.flush();
            }
            catch (Exception e){
                Log.e(Constants.DEBUG_TAG, "Error: ", e);
            }
        }
        return null;
    }
}