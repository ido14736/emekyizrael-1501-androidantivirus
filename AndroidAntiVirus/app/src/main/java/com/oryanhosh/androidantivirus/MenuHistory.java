package com.oryanhosh.androidantivirus;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class MenuHistory extends Fragment{
    private ListView listViewHistory;
    private TextView textViewEmptyHistory;
    private ArrayList<History> histories = new ArrayList<>();

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle("HISTORY");
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.menu_history, container, false);

        listViewHistory = view.findViewById(R.id.listViewHistory);
        textViewEmptyHistory = view.findViewById(R.id.textViewEmptyHistory);

        histories.clear();
        histories = HistoryController.getHistoryItems(getContext());

        if(histories.isEmpty())
        {
            textViewEmptyHistory.setVisibility(View.VISIBLE);
            listViewHistory.setVisibility(View.INVISIBLE);
        }
        else
        {
            HistoryAdapter adapter = new HistoryAdapter(getContext(), R.layout.history_item, histories);
            listViewHistory.setAdapter(adapter);
        }

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.history_menu, menu);
        super.onCreateOptionsMenu(menu,inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.deleteHistoryMenuItem)
        {
            HistoryController.removeAllHistoryItems(getContext());
            textViewEmptyHistory.setVisibility(View.VISIBLE);
            listViewHistory.setVisibility(View.INVISIBLE);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
