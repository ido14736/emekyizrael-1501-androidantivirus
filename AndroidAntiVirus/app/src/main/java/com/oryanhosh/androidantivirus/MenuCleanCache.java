package com.oryanhosh.androidantivirus;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class MenuCleanCache extends Fragment implements View.OnClickListener {
    public static ProgressDialog dialog;
    public static Button buttonCheck;
    public static boolean beforeCheck = true;



    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle("CLEAN CACHE");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.menu_clean_cache, container, false);
        buttonCheck = view.findViewById(R.id.buttonCheckCache);
        buttonCheck.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        if(v == buttonCheck)
        {
            if(beforeCheck)
            {
                dialog = ProgressDialog.show(getContext(), "",
                        "Calculating... ", true);
                MainActivity.startService(Constants.TYPE_EXEC_APPPROCESS + "plugin2,calculateTotalCache", getContext());
            }
            else
            {
                dialog = ProgressDialog.show(getContext(), "",
                        "Deleting... ", true);
                MainActivity.startService(Constants.TYPE_EXEC_APPPROCESS + "plugin2,deleteAllCache", getContext());
            }
            beforeCheck = !beforeCheck;
        }
    }
}
