package com.oryanhosh.androidantivirus;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

class HistoryAdapter extends ArrayAdapter<History> {
    private Context context;
    private int resource;
    private ArrayList<History> histories;

    public HistoryAdapter(@NonNull Context context, int resource, @NonNull ArrayList<History> histories) {
        super(context, resource, histories);
        Collections.sort(histories, new Comparator<History>() {
            @Override
            public int compare(History o1, History o2) {
                return (int) (o2.getTime() - o1.getTime());
            }
        });
        this.context = context;
        this.resource = resource;
        this.histories = histories;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(resource, null);
        TextView textViewTitle = view.findViewById(R.id.textViewDetailTitle);
        TextView textViewContent = view.findViewById(R.id.textViewDetailContent);
        TextView textViewTime= view.findViewById(R.id.textViewDetailTime);
        textViewTime.setText(getDate(histories.get(position).getTime()));
        switch (histories.get(position).getType())
        {
            case Constants.TYPE_DOMAIN:
                textViewTitle.setText("harmful domain");
                break;
            case Constants.TYPE_APP:
                textViewTitle.setText("harmful app");
                break;
            case Constants.TYPE_CAMERA:
                textViewTitle.setText("background camera usage");
                break;
            case Constants.TYPE_APPS:
                textViewTitle.setText("harmful apps");
                break;
            case Constants.TYPE_DOMAINS:
                textViewTitle.setText("harmful domains");
                break;
            case Constants.TYPE_LIBS:
                textViewTitle.setText("harmful files");
                break;
                default:
                    Log.e(Constants.DEBUG_TAG, "unsupported type: " + histories.get(position).getType());
                    break;

        }
        textViewContent.setText(histories.get(position).getData().trim());

        return view;
    }

    private String getDate(long timeStamp){
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
            Date netDate = (new Date(timeStamp));
            return sdf.format(netDate);
        }
        catch(Exception ex){
            return "xx";
        }
    }
}
