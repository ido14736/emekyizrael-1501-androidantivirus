package com.oryanhosh.androidantivirus;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.DataOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private Fragment currFragment = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        displaySelectedScreen(R.id.nav_menu_scan_apps); //display screen

        if (!isMyServiceRunning(this)) { //start background service
            startService(Constants.TYPE_INIT_MESSAGE + "nothing", this);
        }

        LocalBroadcastManager.getInstance(this).registerReceiver(
                broadcastReceiver, new IntentFilter("ServiceAndMainActivityUpdates"));
    }

    /**
     * Function start MyService class
     * input: init message, context
     * output: noun
     * */
    public static void startService(String msg, Context context){
        Intent serviceIntent = new Intent(context, MyService.class);
        serviceIntent.putExtra("inputExtra", msg);
        ContextCompat.startForegroundService(context, serviceIntent);
    }

    private void stopService(){
        Intent serviceIntent = new Intent(this, MyService.class);
        stopService(serviceIntent);
        finish();
    }

    /**
     * Function check if ServiceClass run in the Background
     * input: service class
     * output: true/false
     * */
    static boolean isMyServiceRunning(Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (MyService.class.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    /**
     * BroadcastReceiver receive messages from MyService class
     * */
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, Intent intent) {
            String msg = intent.getStringExtra("inputExtra");
            if(msg != null && !msg.isEmpty()) {
                if(msg.startsWith(Constants.TOBROADCAST_FROM_SERVER))
                {
                    if(MenuScanApps.dialog!=null)
                    {
                        MenuScanApps.dialog.dismiss();
                    }
                    String type = msg.substring(11,13);
                    switch (type)
                    {
                        case "01":
                            msg = "\n" + msg.substring(14);
                            StringBuilder appAndLib = new StringBuilder(msg.split(":")[0]
                                    + " : " + msg.split(":")[1] + "\n");
                            if(msg.contains(","))
                            {
                                appAndLib = new StringBuilder();
                                for(String appLib : msg.split("]"))
                                {
                                    try{
                                        appAndLib.append(appLib.split(":")[0].replaceAll("'", "").replace(",", "")).append(" : ").append(appLib.split(":")[1].replace("'", " ")).append("]\n");
                                    }catch (Exception ignored){}
                                }
                            }
                            buildNotification(context, "\n" + appAndLib.toString().trim(), Constants.TYPE_LIBS);
                            break;
                        case "02":
                            StringBuilder app = new StringBuilder(msg.substring(msg.indexOf("'") + 1, msg.lastIndexOf("'")));
                            if(msg.contains(","))
                            {
                                app = new StringBuilder();
                                for(String pack : msg.substring(msg.indexOf("'")+1, msg.lastIndexOf("'")).split(","))
                                {
                                    app.append(pack.replace("'", "")).append("\n");
                                }
                            }
                            buildNotification(context, "\n" + app.toString().trim(),
                                    app.toString().trim().contains("\n") ? Constants.TYPE_APPS : Constants.TYPE_APP);
                            break;
                        case "03":
                            StringBuilder domain = new StringBuilder(msg.split("'")[1]);
                            if(msg.contains(","))
                            {
                                domain = new StringBuilder();
                                for(String dom : msg.split(","))
                                {
                                    domain.append(dom.substring(dom.indexOf("'") + 1, dom.lastIndexOf("'"))).append("\n");
                                }
                            }
                            buildNotification(context, "\n" + domain.toString().trim() + "\n",
                                    domain.toString().trim().contains("\n") ? Constants.TYPE_DOMAINS: Constants.TYPE_DOMAIN);
                            break;
                            default:
                                break;
                    }
                }
                else if (msg.startsWith(Constants.TOBROADCAST_FROM_CACHE))
                {
                    MenuCleanCache.dialog.dismiss();
                    if(MenuCleanCache.beforeCheck)
                    {
                        MenuCleanCache.buttonCheck.setBackgroundResource(R.drawable.check_cache_button);
                        MenuCleanCache.buttonCheck.setText("");
                        Toast.makeText(context, "Junk cleaned!", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        MenuCleanCache.buttonCheck.setBackgroundResource(R.drawable.empty_button);//.setText(msg.split(":")[1]);
                        String printedMsg = String.valueOf(
                                new DecimalFormat("#.##")
                                        .format(Float.valueOf(msg.split(":")[1].trim())/1000.0))
                                + " MB\ncache";
                        MenuCleanCache.buttonCheck.setText(printedMsg);
                    }
                }
                else if (msg.startsWith(Constants.TOBROADCAST_FROM_DUMP) && !Constants.topApps.isEmpty())
                {
                    msg = msg.substring(9);
                    String conn = msg.split(Pattern.quote("|"))[0];
                    //split connections to string array
                        if (conn.length() < 10){
                            return;
                        }
                        boolean newConn = conn.charAt(conn.indexOf("CONNECT")-1) == ' ';
                        String packageName = conn.substring(conn.indexOf("package") + 8).split(" ")[0].trim();

                        if(!newConn) {
                            return;
                        }

                    ArrayList<String> appsInUi = new ArrayList<>(Constants.topApps);
                        if(!appsInUi.isEmpty())
                        {
                            if(!appsInUi.contains(packageName))
                            {
                                Log.i(Constants.DEBUG_TAG, packageName + "using camera but not alive");
                                buildNotification(context, packageName, Constants.TYPE_CAMERA);
                            }
                            else
                            {
                                Log.i(Constants.DEBUG_TAG, packageName + " using camera in ui");
                            }
                        }
                    Constants.topApps.clear();
                    }
                }
            }
        };

    /**
     * Function switch fragment to display
     * input: navigation menu item
     * output: noun
     * */
    private void displaySelectedScreen(int id) {

        switch (id)
        {
            case R.id.nav_menu_scan_apps:
                currFragment = new MenuScanApps();
                break;
            case R.id.nav_menu_scan_libs:
                currFragment = new MenuScanLibs();
                break;
            case R.id.nav_menu_clean_cache:
                currFragment = new MenuCleanCache();
                break;
            case R.id.nav_menu_about_device:
                currFragment = new MenuYourDevice();
                break;
            case R.id.nav_menu_history:
                currFragment = new MenuHistory();
                break;
            case R.id.nav_menu_disabled_apps:
                currFragment = new MenuDisabledApps();
                break;
        }
        if (currFragment != null){
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_main,  currFragment);
            ft.commit();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings1) {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP)
            {
                Intent intent = new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS);
                startActivity(intent);
            }
            return true;
        }
        else if (id == R.id.action_settings2) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(this)) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                        Uri.parse("package:" + getPackageName()));
                startActivityForResult(intent, 1);
            } else
            {
                Toast.makeText(this, "No action is needed", Toast.LENGTH_SHORT).show();
            }
            return true;
        }
        else if (id == R.id.action_stop_service) {
            stopService();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        displaySelectedScreen(id); //replace screen

        return true;
    }


    private void buildNotification(final Context context, final String packageName, final int msgType)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(this)) {
            return;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(context.getApplicationContext());
        LayoutInflater inflater = LayoutInflater.from(context);
        View dialogView = inflater.inflate(R.layout.alert_popup, null);

        TextView textViewTitle = dialogView.findViewById(R.id.textViewAlertTitle);
        TextView textViewAppName = dialogView.findViewById(R.id.textViewAlertAppName);
        TextView textViewAllow = dialogView.findViewById(R.id.textViewAlertAllow);
        Button buttonDisableApp = dialogView.findViewById(R.id.buttonAlertDisableApp);
        ImageView imageViewIcon = dialogView.findViewById(R.id.imageViewAlertAppIcon);

        StringBuilder pack = new StringBuilder();
        if(msgType == Constants.TYPE_APPS)
        {
            for(String app : packageName.split("\n"))
            {
                if(app.length() < 4)
                {
                    continue;
                }
                pack.append(getAppNameByPackage(context, app.trim())).append("\n");
            }
            pack = new StringBuilder("\n" + pack.toString().trim());
        }
        else if(msgType == Constants.TYPE_LIBS)
        {
            for(String app : packageName.split("\n"))
            {
                if(app.length() < 4)
                {
                    continue;
                }
                pack.append(getAppNameByPackage(context, app.split(":")[0].trim())).append(app.split(":")[1]).append("\n");
            }
            pack = new StringBuilder("\n" + pack.toString().trim());
        }
        else
        {
            pack = new StringBuilder(getAppNameByPackage(context, packageName));
        }
        switch (msgType)
        {
            case Constants.TYPE_CAMERA:
                textViewTitle.setText(getString(R.string.title_type_camera));
                buttonDisableApp.setText(getString(R.string.disable_app_action));
                textViewAllow.setText(getString(R.string.do_nothing_camera_action));
                textViewAppName.setText(pack.toString());
                HistoryController.addHistoryItem(context,
                        new History(System.currentTimeMillis(), Constants.TYPE_CAMERA, pack.toString()));
                break;
            case Constants.TYPE_APP:
                textViewTitle.setText(getString(R.string.title_type_app));
                buttonDisableApp.setText(getString(R.string.disable_app_action));
                textViewAllow.setText(getString(R.string.do_nothing_action));
                textViewAppName.setText(pack.toString());
                HistoryController.addHistoryItem(context,
                        new History(System.currentTimeMillis(), Constants.TYPE_APP, pack.toString()));
                break;
            case Constants.TYPE_APPS:
                textViewTitle.setText(getString(R.string.title_type_apps));
                buttonDisableApp.setText(getString(R.string.disable_apps_action));
                textViewAllow.setText(getString(R.string.do_nothing_action));
                textViewAppName.setText(pack.toString());
                HistoryController.addHistoryItem(context,
                        new History(System.currentTimeMillis(), Constants.TYPE_APPS, pack.toString()));
                break;
            case Constants.TYPE_DOMAIN:
                textViewTitle.setText(getString(R.string.title_type_domain));
                textViewAppName.setText(packageName);
                buttonDisableApp.setVisibility(View.GONE);
                imageViewIcon.setVisibility(View.GONE);
                textViewAllow.setVisibility(View.GONE);
                HistoryController.addHistoryItem(context,
                        new History(System.currentTimeMillis(), Constants.TYPE_DOMAIN, packageName));
                break;
            case Constants.TYPE_DOMAINS:
                textViewTitle.setText(getString(R.string.title_type_domains));
                textViewAppName.setText(packageName);
                buttonDisableApp.setVisibility(View.GONE);
                imageViewIcon.setVisibility(View.GONE);
                textViewAllow.setVisibility(View.GONE);
                HistoryController.addHistoryItem(context,
                        new History(System.currentTimeMillis(), Constants.TYPE_DOMAINS, packageName));
                break;
            case Constants.TYPE_LIBS:
                textViewTitle.setText(getString(R.string.title_type_libs));
                textViewAppName.setText(pack.toString());
                buttonDisableApp.setText(getString(R.string.disable_apps_action));
                imageViewIcon.setVisibility(View.GONE);
                textViewAllow.setText(getString(R.string.do_nothing_action));
                HistoryController.addHistoryItem(context,
                        new History(System.currentTimeMillis(), Constants.TYPE_LIBS, pack.toString()));
                break;
            default:
                Log.e(Constants.DEBUG_TAG, "unrecognized msg type: " + String.valueOf(msgType));
                break;
        }

        builder.setView(dialogView);

        if(Constants.alert != null)
        {
            Constants.alert.cancel();
        }

        Constants.alert = builder.create();
        Constants.alert.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        Constants.alert.getWindow().setType(WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY);
        Constants.alert.setCancelable(false);
        try
        {
            Drawable drawable = getPackageManager().getApplicationIcon(packageName.trim());
            imageViewIcon.setImageDrawable(drawable);
        } catch (PackageManager.NameNotFoundException e)
        {
            imageViewIcon.setVisibility(View.GONE);
        }
        buttonDisableApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(msgType == Constants.TYPE_LIBS || msgType == Constants.TYPE_APPS)
                {
                    for(String app : packageName.split("\n"))
                    {
                        if(app.length() < 4)
                        {
                            continue;
                        }
                        String appPackage = app.split(":")[0].trim();
                        disableApp(context, appPackage);
                    }
                    Toast.makeText(context, "apps disabled", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    disableApp(context, packageName);
                    Toast.makeText(context, "app disabled", Toast.LENGTH_SHORT).show();
                }
                Constants.alert.cancel();
                Constants.alert = null;
            }
        });
        (dialogView.findViewById(R.id.imageViewAlertCancel)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.alert.cancel();
                Constants.alert = null;
            }
        });
        textViewAllow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.alert.cancel();
                Constants.alert = null;
            }
        });

        Constants.alert.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = Constants.alert.getWindow();
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    public static String getAppNameByPackage(Context context, String packageName)
    {
        PackageManager packageManager = context.getPackageManager();
        ApplicationInfo applicationInfo = null;
        try {
            applicationInfo = packageManager.getApplicationInfo(packageName, 0);
        } catch (final PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return (String)((applicationInfo != null) ? packageManager.getApplicationLabel(applicationInfo) : packageName);
    }

    private void disableApp(Context context, String packageName)
    {
        try {
            Process p = Runtime.getRuntime().exec("su");
            DataOutputStream dos = new DataOutputStream(p.getOutputStream());
            dos.writeBytes("pm disable " + packageName.trim() + "\n");
            dos.writeBytes("exit\n");
            dos.flush();
            dos.close();
            DisabledAppsController.addDisabledAppItem(context, packageName);
        } catch (IOException e){
            e.printStackTrace();
        }
    }
}