package com.oryanhosh.androidantivirus;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.util.Log;

public class BroadcastReceiverPackageChange extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        //check if new package installed
        if ("android.intent.action.PACKAGE_ADDED".equals(intent.getAction()))
        {
            Log.i(Constants.DEBUG_TAG, "PACKAGE_ADDED: " + intent.getDataString());
            Intent serviceIntent = new Intent(context, MyService.class);
            serviceIntent.putExtra("inputExtra", Constants.TYPE_CHECK_PACKAGE + intent.getDataString());
            ContextCompat.startForegroundService(context, serviceIntent);
        }
    }
}
